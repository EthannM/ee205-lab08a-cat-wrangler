#pragma once
#include "node.hpp"


using namespace std;


//static unsigned int countt;

class DoubleLinkedList {
protected:
Node* head = nullptr;
Node* tail = nullptr;
public:
//old functions
const bool empty() const;
void push_front(Node* newNode);
Node* pop_front();
Node* get_first() const;
Node* get_next(const Node* currentNode) const;
inline unsigned int size() const {return countt;};

//new functions
void push_back(Node* newNode);
Node* pop_back();
Node* get_last()const;
Node* get_prev(const Node* currentNode) const;

//insert after/before
void insert_after(Node* currentNode, Node* newNode); 
void insert_before(Node* currentNode, Node* newNode);

//helper
const bool isIn(Node* aNode) const;
bool validate() const;
void dump() const;
//count
unsigned int countt;
//swap
void swap(Node* node1, Node* node2);
const bool isSorted() const;
void insertionSort();
};


