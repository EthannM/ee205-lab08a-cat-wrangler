#include <iostream>
#include <cassert>

#include "list.hpp"
#include "node.hpp"



using namespace std;
//swap

void DoubleLinkedList :: swap(Node* node1, Node* node2)
{
//if size <2 
if(size() <2){
return;
}

//if input is nullptr
if(node1 == nullptr || node2 == nullptr){
return;
}

//swap same nodes
if(node1 == node2){
return;

}


//swap first and last
if(node1 == head && node2 == tail){
   if(DoubleLinkedList::size() == 2){
      head = node1;
      tail = node1;
      head -> next = nullptr;
      head -> prev = nullptr;
      tail -> next = nullptr;
      tail -> prev = nullptr;

      DoubleLinkedList :: push_back(node1);


   }
else{
DoubleLinkedList :: pop_front();
DoubleLinkedList :: push_front(node2);

DoubleLinkedList :: pop_back();
DoubleLinkedList :: push_back(node1);

return;
}
}
if(node1 == tail && node2 == head){
   if(DoubleLinkedList::size() == 2){
      head = node1;
      tail = node1;
      head -> next = nullptr;
      head -> prev = nullptr;
      tail -> next = nullptr;
      tail -> prev = nullptr;

      DoubleLinkedList :: push_back(node2);
   }
else{
DoubleLinkedList :: pop_front();
DoubleLinkedList :: push_front(node1);

DoubleLinkedList :: pop_back();
DoubleLinkedList :: push_back(node2);

return;
}
}

//swap head with upcoming
if(node1 == head || node2 == head){
Node* currentNode = head;


   if (node1 == head){
    while(currentNode != node2){
   currentNode = currentNode -> next;
   }
   Node* swap = currentNode; 
   currentNode = swap -> next;
   swap -> prev -> next = currentNode; 
   delete swap;
   countt --;
   DoubleLinkedList :: insert_before(currentNode, node1);

   DoubleLinkedList :: pop_front();
   DoubleLinkedList :: push_front(node2);
   return;
   }

   if(node2 == head){
    while(currentNode != node1){
   currentNode = currentNode -> next;
   }
   Node* swapp = currentNode;
   currentNode = swapp -> next;
   swapp -> prev -> next = currentNode;
   delete swapp;
   countt --;
   DoubleLinkedList:: insert_before(currentNode, node2);

   DoubleLinkedList ::pop_front();
   DoubleLinkedList :: push_front(node1);
   return;

   }

}


//swap tail with upcoming
if(node1 == tail || node2 == tail){
Node* currentNode = head;


   if (node1 == tail){
    while(currentNode != node2){
   currentNode = currentNode -> next;
   }
   Node* swap = currentNode;
   currentNode = swap -> next;
   swap -> prev -> next = currentNode;
   delete swap;
   countt --;
   DoubleLinkedList:: insert_before(currentNode, node1);

   DoubleLinkedList :: pop_back();
   DoubleLinkedList :: push_back(node2);
   return;
   }

   if(node2 == tail){
    while(currentNode != node1){
   currentNode = currentNode -> next;
   }
   Node* swapp = currentNode;
   currentNode = swapp -> next;
   swapp -> prev -> next = currentNode;
   delete swapp;
   countt --;
   DoubleLinkedList ::insert_before(currentNode, node2);

   DoubleLinkedList ::pop_back();
   DoubleLinkedList :: push_back(node1);
   return;

   }

}



//swap next to each other
else{
Node* currentNode = head; 
 while(currentNode != node2){
   currentNode = currentNode -> next;
   }
   Node* swap = currentNode;
   currentNode = swap -> next;
   swap -> prev -> next = currentNode;
   delete swap;
   countt --;
   DoubleLinkedList:: insert_before(currentNode, node1);


   while(currentNode != node1){
   currentNode = currentNode -> next;
   }
   Node* swapp = currentNode;
   currentNode = swapp -> next;
   swapp -> prev -> next = currentNode;
   delete swapp;
   countt --;
   DoubleLinkedList ::insert_before(currentNode, node2);

return;
}

}
/*
 const bool isSorted() const{
if(countt <= 1)
return true;

for( Node* i = head; i -> next != nullptr; i = i -> next){
   if(*i > *i -> next)
      return false;

}
return true;

}


void insertionSort(){
if(DoubleLinkedList::isSorted()){

   return;
}

Node* currentNode = head;
Node* prevNode = head;
while(DoubleLinkedList:: isSorted() == false){
Node* currentNode = head;
Node* prevNode = head;

   while( currentNode  != nullptr){
   prevNode = currentNode;
   currentNode -> next = currentNode;
   if(currentNode > prevNode){
   return;
   }

   else{
   DoubleLinkedList::swap(currentNode, prevNode);
   }

   }
}


}

*/








//Basic functions from Single linked list
//check if list is empty 
const bool DoubleLinkedList ::empty() const
{
   if (head == nullptr && tail == nullptr)
      return true;
   else
      return false;


}
//add newNode to front of list
void DoubleLinkedList :: push_front(Node* newNode)
{
   if(newNode == nullptr)
   {
      return;
   }
 
   if(head!= nullptr)
   {
      newNode -> next = head;
      newNode -> prev = nullptr;
      head -> prev =  newNode;
      head = newNode;
      
   }
   else
   {
   newNode -> next = nullptr;
   newNode -> prev = nullptr;
   head = newNode;
   tail = newNode;


   }
   countt ++;
  // validate();

}


//remove node from front of the list
Node* DoubleLinkedList ::pop_front()
{

   if(head == nullptr && tail == nullptr)
   {
      return nullptr;
   }

   if(head -> next == nullptr || tail -> prev == nullptr )
   {  

      head = nullptr;
      tail = nullptr;
      countt --;
      return nullptr;
      

   }

   else
   {
     Node* returnValue = head;
     head = returnValue -> next;
     head -> prev = nullptr;
     delete returnValue;
     countt --;
     return nullptr;
   }


}
//return first node
Node* DoubleLinkedList ::get_first() const
{
   if(head == nullptr && tail == nullptr){
   return nullptr;
   }
   else{
   return head;
   }
}


//return the node following the current node
Node* DoubleLinkedList ::get_next(const Node* currentNode) const
{
if(currentNode == nullptr && head == nullptr){
   
   return nullptr;
}

if(currentNode -> next == nullptr){
   return nullptr;
}

else{
Node* nextNode;
nextNode = currentNode -> next;
return nextNode;
}



//validate();

}


//new functions for double linked list 
//add Newnode to the back of the list 
void DoubleLinkedList :: push_back(Node* newNode)
{
if(newNode == nullptr)
   {
      return;
   }

   if(head != nullptr )
   {
      newNode -> prev = tail;
      newNode -> next = nullptr;
      tail -> next =  newNode;
      tail = newNode;

   }
   else
   {
   newNode -> next = nullptr;
   newNode -> prev = nullptr;
   head = newNode;
   tail = newNode;


   }
   countt ++;
   




}
//remove node from the back of the list
Node* DoubleLinkedList :: pop_back()
{
 if(head == nullptr && tail == nullptr)
   {
      return nullptr;
   }

   if(head -> next == nullptr || tail -> prev == nullptr )
   {

      head = nullptr;
      tail = nullptr;
      countt --;
      return nullptr;


   }

   else
   {
     Node* returnValue = tail;
     tail = returnValue -> prev;
     tail -> next = nullptr;
     delete returnValue;
     countt --;
     return returnValue;
   }



}

//return very last node in the list 
Node* DoubleLinkedList :: get_last() const
{
   return tail;   
}


Node*  DoubleLinkedList :: get_prev(const Node* currentNode) const
{
   Node* prevNode;
   prevNode = currentNode -> prev;
   return prevNode;

}


//Insert Newnode right after current node 
void DoubleLinkedList :: insert_after(Node* currentNode, Node* newNode)
{

   
   if(currentNode == nullptr && head == nullptr){
      push_front(newNode);
      return; 
   }
   
   if(tail != currentNode){
      newNode ->next = currentNode -> next;
      currentNode -> next = newNode;
      newNode  ->prev = currentNode;
      newNode -> next ->prev = newNode;
      countt ++;
      }
   else{
      push_back(newNode);
      
}

}


//insert Newnode right before current node
void DoubleLinkedList :: insert_before(Node* currentNode, Node* newNode)
{

 if(currentNode == nullptr && head == nullptr){
      push_front(newNode);
      return;
   }

   if(head != currentNode){
      newNode ->prev = currentNode -> prev;
      currentNode -> prev = newNode;
      newNode  ->next = currentNode;
      newNode -> prev ->next = newNode;
      countt ++;
      }
   else{
      push_front(newNode);

      
}


} 





const bool DoubleLinkedList::isIn(Node* aNode) const
{
Node* currentNode = head;

while(currentNode != nullptr){
   if(aNode == currentNode)
      return true;
   currentNode = currentNode ->next;

}
   return false;
}

bool DoubleLinkedList::validate() const
{
   if(head == nullptr){
      assert(tail == nullptr);
      assert(countt == 0);
   } else{
      assert(tail != nullptr);
      assert (countt != 0);
   }
   if(tail ==nullptr){
      assert(head == nullptr);
      assert(countt == 0);
   } else{
      assert(head != nullptr);
      assert(countt != 0);
   }
   if(head != nullptr && tail == head){
      assert(countt == 1);
   }

   unsigned int forwardCount = 0;
   Node* currentNode = head;
    //Count forward through the list
   while(currentNode != nullptr){
      forwardCount++;
      if(currentNode -> next != nullptr){
         assert(currentNode -> next -> prev == currentNode);
      }
      currentNode = currentNode -> next;
}
cout << forwardCount << endl;
assert(countt == forwardCount);

//Count backward through the list 
unsigned int backwardCount = 0;
currentNode = tail;
while(currentNode != nullptr){
   backwardCount++;
   if( currentNode -> prev != nullptr){
      assert( currentNode ->prev->next == currentNode);
   }
   currentNode = currentNode ->prev;
}
assert(countt == backwardCount);

return true;

}


void DoubleLinkedList:: dump() const{
cout << "DoubleLinkedList:  head=[" << head <<"] tail =[" << tail <<"]" << endl;for(Node* currentNode = head ; currentNode != nullptr ; currentNode = currentNode -> next){
cout << "  ";
currentNode -> dump();
}
}




